<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\Achievement as AchievementResource;
use \App\Http\Resources\UserData as UserDataResource;
use \App\Http\Resources\Friend as FriendResource;
use App\Achievement;
use \App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('/login', 'AuthController@login');
    Route::post('/signup', 'AuthController@signup');

    /** Header with {Authorization: type token} required */
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::post('/logout', 'AuthController@logout');
        Route::get('/user', 'AuthController@user');
    });
});


/** GET REQUESTS */

Route::get('/user_data', function (Request $request) {
    $user_id = $request->user()->id;
    return new UserDataResource(User::find($user_id));
})->middleware("auth:api");

Route::get('/friend_data/{id}', function ($id) {
    return new FriendResource(User::find($id));
});

Route::get('/users/{pattern}', 'UserController@get_users');

/** FRIENDSHIPS REQUESTS */
Route::get('/friendship', 'FriendshipController@allRequestsOfUser')->middleware("auth:api");

Route::post('/friendship', 'FriendshipController@store')->middleware("auth:api");

Route::put('/friendship/{friendship}', 'FriendshipController@update')->middleware("auth:api");;

Route::delete('/friendship/{friendship}', 'FriendshipController@delete')->middleware("auth:api");;

/** ACHIEVEMENT REQUESTS */

Route::get('/achievements/', function () {
    return AchievementResource::collection(Achievement::all());
});

Route::get('/achievement/{id}/', function ($id) {
    return new AchievementResource(Achievement::find($id));
});

Route::post('/achievement/', 'AchievementController@store')->middleware("auth:api");;

Route::delete('/achievement/{achieved}', 'AchievementController@delete')->middleware("auth:api");;

/** PROFILE REQUESTS */

Route::post('/profile', 'ProfileController@update_avatar')->middleware("auth:api");;

Route::get('/profile/{id}', 'ProfileController@get_profile_pic');

/** REST REQUESTS */

Route::post('/rest/', 'RestController@store')->middleware("auth:api");

Route::put('/rest/{rest}', 'RestController@update')->middleware("auth:api");;

Route::delete('/rest/{rest}', 'RestController@delete')->middleware("auth:api");;

/** DREAM REQUESTS */

Route::post('/dream/', 'DreamController@store')->middleware("auth:api");;

Route::put('/dream/{dream}', 'DreamController@update')->middleware("auth:api");;

Route::delete('/dream/{dream}', 'DreamController@delete')->middleware("auth:api");;

