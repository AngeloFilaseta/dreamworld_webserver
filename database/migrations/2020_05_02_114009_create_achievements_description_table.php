<?php

use App\Achievement;
use App\AchievementDescription;
use App\AchievementTitle;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAchievementsDescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievements_description', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('achievement_id');
            $table->text('eng');
            $table->text('ita');
            $table->foreign('achievement_id')->references('id')->on('achievements')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });

        $this->add_achievement("Sorridi!","Smile!",
            "Imposta una foto profilo.","Set a profile picture.",1,2);

        $this->add_achievement("L'amicizia è per sempre!","Friendship is forever!",
            "Invia una richiesta di amicizia.","Send a friend request.",1,2);

        $this->add_achievement("Sto sognando?","Am I dreaming?",
            "Imposta una notifica promemoria.","Set a reminder notification.",1,2);

        $this->add_achievement("Sveglia!","Wake Up!",
            "Imposta una sveglia.","Set an alarm.",1,2);

        $this->add_achievement("Principiante","Beginner",
            "Annota il tuo primo sogno.","Write down your first dream.",1,2);

        $this->add_achievement("Le basi della lucidità","The basis of lucidity",
            "Leggi la prima guida Dreamworld.","Read the first Dreamworld guide.",1,5);

        $this->add_achievement("Tecnica DILD","DILD Technique",
            "Leggi la seconda guida Dreamworld.","Read the second Dreamworld guide.",1,5);

        $this->add_achievement("Tecnica MILD","MILD Technique",
            "Leggi la terza guida Dreamworld.","Read the third Dreamworld guide.",1,5);

        $this->add_achievement("Tecnica ADA","ADA Technique",
            "Leggi la quarta guida Dreamworld.","Read the fourth Dreamworld guide.",1,5);

        $this->add_achievement("Ce l'ho fatta!","I did it!",
            "Fai il tuo primo sogno lucido.","Have your first lucid dream.",2,10);

        $this->add_achievement("Apprendista","Apprentice",
            "Annota 25 sogni.","Write down 25 dreams.",2,10);

        $this->add_achievement("Gemma della Realtà","Reality Stone",
            "Modifica un sogno usando la tua volontà.","Influence a dream using your will.",2,15);

        $this->add_achievement("Gemma del tempo","Time stone",
            "Viaggia nel tempo.","Accomplish Time Travel.",2,15);

        $this->add_achievement("Gemma dello spazio","Space Stone",
            "Viaggia in un posto lontano in un sogno.","Travel to a foreign place in dream.",2,15);

        $this->add_achievement("Gemma dell'anima","Soul Stone",
            "Parla con una persona defunta in un sogno.","Talk to a dead person in dreams.",2,15);

        $this->add_achievement("Gemma della mente","Mind Stone",
            "Controlla una persona in un sogno.","Control a person in a dream.",2,15);

        $this->add_achievement("Gemma del potere","Power Stone",
            "Abbatti un palazzo in un sogno.","Tear down a building in a dream.",2,15);

        $this->add_achievement("Leggero come una piuma.","Light as a feather",
            "Impara a volare nei sogni.","Learn to fly in dreams.",3,30);
        $this->add_achievement("Heilà!","Hello there!",
            "Sogna una battaglia con spade laser.","Dream a lightsaber fight.",3,30);
        $this->add_achievement("Esperto","Expert",
            "Appunta 50 sogni.","Write down 50 dreams.",3,50);

        $this->add_achievement("Guanto dell'infinito.","Infinity Gauntlet.",
            "Completa tutti gli gli obiettivi delle gemme.",
            "Complete all stone related achievements.",3,100);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achievements_description');
    }

    private function add_achievement($title_it, $title_en, $desc_it, $desc_en, $diff, $exp){
        $achievement = new Achievement();
        $achievement->exp = $exp;
        $achievement->difficulty = $diff;
        $achievement->save();
        $achievement_id = $achievement->id;
        $achievement_desc = new AchievementDescription();
        $achievement_desc->achievement_id = $achievement_id;
        $achievement_desc->ita = $desc_it;
        $achievement_desc->eng = $desc_en;
        $achievement_desc->save();
        $achievement_title = new AchievementTitle();
        $achievement_title->achievement_id = $achievement_id;
        $achievement_title->ita = $title_it;
        $achievement_title->eng = $title_en;
        $achievement_title->save();
    }



}
