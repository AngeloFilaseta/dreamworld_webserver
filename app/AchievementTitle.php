<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AchievementTitle extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'achievements_title';
}
