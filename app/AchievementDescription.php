<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AchievementDescription extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'achievements_description';

}
