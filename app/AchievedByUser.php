<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AchievedByUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'achieved_by_users';

}
