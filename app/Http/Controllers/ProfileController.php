<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;


class ProfileController extends Controller
{

    public function get_profile_pic($user_id){
        $path = public_path().'/images/user.png';

        $user = User::find($user_id);
        if($user == null){
            return response()->download($path);
        }
        $avatar = $user->avatar;
        if($avatar != "user.png"){
            $path = public_path().'/storage/avatars/'.$avatar;
        }
        if(file_exists($path)){
            return response()->download($path);
        }
        return response()->json([
            'message' => 'File not found.'], 404);
    }

    public function update_avatar(Request $request){

        /*$request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]); */
        
        $user = $request->user();

        $avatarName = $user->id.'_avatar.png';
        $user->avatar = $avatarName;
        $user->save();

        $file = base64_decode($request->avatar);
        $folderName = 'storage/avatars/';
        $avatarName = $user->id.'_avatar.png';
        $destinationPath = public_path() ."/". $folderName;
        file_put_contents($destinationPath.$avatarName, $file);

        return response()->json(["message" => "Image File uploaded successfully."], 200);
    }

}
