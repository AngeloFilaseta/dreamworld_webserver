<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function get_users(Request $request, $pattern)
    {
        $users = User::where('username', 'LIKE', '%'.$pattern.'%')->get();
        return response()->
            json(['data' => $users], 200);
    }
}
