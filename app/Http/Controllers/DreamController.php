<?php

namespace App\Http\Controllers;

use App\Dream;
use App\Rest;
use Illuminate\Http\Request;

class DreamController extends Controller
{
    public function store(Request $request)
    {
        $rest = Rest::find($request->rest_id);
        if($rest->user_id == $request->user()->id){
            $dream = new Dream();
            $dream->rest_id = $request->rest_id;
            $dream->title = $request->title;
            $dream->description = $request->description;
            $dream->lucid = $request->lucid;
            $dream->save();
            return response()->json(new \App\Http\Resources\Dream($dream), 201);
        }
        return response()->json(['message' => $rest], 401);
    }

    public function update(Request $request, Dream $dream)
    {
        $dream->update($request->all());
        return response()->json($dream, 200);
    }

    public function delete(Request $request, Dream $dream)
    {
        $dream->delete();

        return response()->json(null, 204);
    }
}
