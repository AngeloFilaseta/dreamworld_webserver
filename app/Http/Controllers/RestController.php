<?php

namespace App\Http\Controllers;

use App\Rest;
use App\Dream;

use Illuminate\Http\Request;

class RestController extends Controller
{
    public function store(Request $request)
    {
        if (Rest::where('date', '=', $request->date)->where('fall_asleep_time', '=', $request->fall_asleep_time)->
                where('user_id', '=', $request->user()->id)->count() > 0) {
            return response()->json([
                'message' => 'Conflict, this rest alredy exists.'], 409);
        }
        $rest = new Rest();
        $rest->user_id = $request->user()->id;
        $rest->date = $request->date;
        $rest->fall_asleep_time = $request->fall_asleep_time;
        $rest->wake_up_time = $request->wake_up_time;
        $rest->save();
        $dreams = $request->dreams;
        /*$array = $dreams->values;
         foreach($array as $elem){
         $dreamElem = $elem->nameValuePairs;
         $dream = new Dream();
         $dream->rest_id = $rest->id;;
         $dream->title = $dreamElem["title"];
         $dream->description = $dreamElem["description"];
         $dream->lucid = $dreamElem["lucid"];
         $dream->save(); */
        foreach ($dreams as $d){
            $dream = new Dream();
            $dream->rest_id = $rest->id;;
            $dream->title = $d["title"];
            $dream->description = $d["description"];
            $dream->lucid = $d["lucid"];
            $dream->save();
            }

        return response()->json(new \App\Http\Resources\Rest($rest), 201);
    }

    public function update(Request $request, Rest $rest)
    {
        $rest_user = $rest->user()->first();
        if($rest_user->id == $request->user()->id){
            $rest->update($request->all());
            return response()->json($rest, 200);
        }
        return response()->json([
            'message' => "Unauthorized"], 401);
    }

    public function delete(Request $request, Rest $rest)
    {
        $rest_user = $rest->user()->first();
        if($rest_user->id == $request->user()->id){
            $rest->delete();
            return response()->json(null, 204);
        }

        return response()->json([
            'message' => "Unauthorized"], 401);
    }
}
