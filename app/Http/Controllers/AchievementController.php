<?php

namespace App\Http\Controllers;

use App\AchievedByUser;

use App\Achievement;
use Illuminate\Http\Request;

class AchievementController extends Controller
{
    public function store(Request $request)
    {
        $check_alredy_present = AchievedByUser::where('user_id', $request->user()->id)
            ->where('achievement_id', $request->achievement_id)->get();
        if($check_alredy_present->count() > 0){
            return response()->json([
                'message' => 'Conflict, Achievement alredy present.'], 409);
        }
        $achieved = new AchievedByUser();
        $achieved->user_id = $request->user()->id;
        $achieved->achievement_id = $request->achievement_id;
        $achieved->save();
        return response()->json($achieved, 201);
    }

    public function delete(Request $request, int $achievement)
    {
        $achieved = AchievedByUser::where('user_id', $request->user()->id)
            ->where('achievement_id', $achievement)->get();
        if($achieved->count() > 0){
            $ach_to_delete = $achieved->first();
            $ach_to_delete->delete();
            return response()->json([
                'message' => "Deleted."], 204);
        }
        return response()->json([
            'message' => "Unauthorized"], 401);
    }
}
