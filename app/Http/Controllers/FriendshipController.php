<?php

namespace App\Http\Controllers;

use App\Friendship;
use Illuminate\Http\Request;

class FriendshipController extends Controller
{
    public function store(Request $request)
    {
        $sender_id = $request->user()->id;
        // Check if the request is alredy present
        $check_alredy_present = Friendship::where('sender_id', $sender_id)
            ->where('receiver_id', $request->receiver_id)->get();
        if($check_alredy_present->count() > 0){
            return response()->json([
                'message' => 'Conflict, Request alredy sent.'], 409);
        }

        // If the friend alredy sent the same request
        $check_alredy_received = Friendship::where('sender_id', $request->receiver_id)
            ->where('receiver_id', $sender_id)->where("accepted", 0)->get();

        if($check_alredy_received->count() > 0){
            $friendship = $check_alredy_received->first();
            $friendship->accepted = 1;
            $friendship -> save();
            return response()->json(["message" => "Accepting the inverse request." , $friendship], 200);
        }

        $friendship = new Friendship();
        $friendship->sender_id = $sender_id;
        $friendship->receiver_id = $request->receiver_id;
        $friendship->accepted = 0;
        $friendship->save();

        return response()->json($friendship, 201);
    }

    public function update(Request $request, $friendID)
    {
        $friendships = Friendship::where('receiver_id', $request->user()->id)
            ->where('sender_id', $friendID)->get();
        if($friendships->count() > 0 ){
            $friendship = $friendships->first();
            $friendship->accepted = 1;
            $friendship->save();
            return response()->json($friendship, 200);
        }
        return response()->json(['message' => 'No data found for this friendship.'], 404);
    }

    public function delete(Request $request, $friendID)
    {
        $friendships_v1 = Friendship::where('receiver_id', $request->user()->id)
            ->where('sender_id', $friendID)->get();
        $friendships_v2 = Friendship::where('receiver_id', $friendID)
            ->where('sender_id', $request->user()->id)->get();
        if($friendships_v1->count() > 0){
            $friendship = $friendships_v1->first();
        }else if($friendships_v2->count() > 0){
            $friendship = $friendships_v2->first();
        }else{
            return response()->json(['message' => 'No data found for this friendship.'], 404);
        }
        $friendship->delete();
        return response()->json(null, 204);
    }

    public function allRequestsOfUser(Request $request)
    {
        $user = $request->user();

        return response()->json(["data" => $user->notAcceppetedRequests()], 200);
    }
}
