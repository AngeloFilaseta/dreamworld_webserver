<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Dream extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'rest_id' => $this->rest_id,
            'lucid' => $this->lucid,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
