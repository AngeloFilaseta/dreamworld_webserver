<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\AchievementTitle as AchievementTitleResource;
use App\Http\Resources\AchievementTitle as AchievementDescriptionResource;

class Achievement extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'difficulty' => $this->difficulty,
            'exp' => $this->exp,
            'title' => new AchievementTitleResource($this->title),
            'description' => new AchievementDescriptionResource($this->description),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
