<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Friend extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' => $this->id,
            'username' => $this->username,
            'total_dream_count' => $this->dreams()->count(),
            'lucid_dream_count' => $this->lucidDreams()->count(),
            'achievements' => Achieved::collection($this->achieved),
            'avatar' => $this->avatar
            ];
    }
}
