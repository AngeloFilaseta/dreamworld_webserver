<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Dream as DreamResource;

class Rest extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'fall_asleep_time' => $this->fall_asleep_time,
            'wake_up_time' => $this->wake_up_time,
            'dreams' => DreamResource::collection($this->dreams),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
