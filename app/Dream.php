<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dream extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dreams';

    protected $fillable = ['title','description','lucid'];

    public function rest(){
        return $this->belongsTo('App\Rest', 'rest_id');
    }

    public function user(){
        return $this->hasOneThrough('App\User', 'App\Rest');
    }

}
