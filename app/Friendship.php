<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friendship extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'friendships';

}
