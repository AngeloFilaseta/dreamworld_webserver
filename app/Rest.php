<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rest extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rests';

    protected $fillable = ['date','fall_asleep_time','wake_up_time'];

    public function dreams(){
        return $this->hasMany('App\Dream');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
