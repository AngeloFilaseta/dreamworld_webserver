<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Achievement extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'achievements';

    protected function title(){
        return $this->hasOne('App\AchievementTitle');
    }
    protected function description(){
        return $this->hasOne('App\AchievementDescription');
    }

}
