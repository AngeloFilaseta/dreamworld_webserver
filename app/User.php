<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function achieved(){
        return $this->hasMany('App\AchievedByUser','user_id');
    }

    public function rests(){
        return $this->hasMany('App\Rest');
    }

    public function dreams(){
        return $this->hasManyThrough('App\Dream', //ok
            'App\Rest', // ok
            'user_id', //should be ok
            'rest_id',
            'id',
            'id')->get();
    }

    public function lucidDreams(){
        return $this->hasManyThrough('App\Dream', //ok
            'App\Rest', // ok
            'user_id', //should be ok
            'rest_id',
            'id',
            'id')->where('lucid', 1)->get();
    }

    public function friends(){
        $sendedRequests = $this->hasManyThrough('App\User',
            'App\Friendship',
            'receiver_id',
            'id',
            'id',
            'sender_id')->where('accepted', 1)->get();

        $receivedRequests = $this->hasManyThrough('App\User',
            'App\Friendship',
            'sender_id',
            'id',
            'id',
            'receiver_id')->where('accepted', 1)->get();

        return $sendedRequests->merge($receivedRequests);
    }

    public function notAcceppetedRequests(){
        return $this->hasManyThrough('App\User',
            'App\Friendship',
            'receiver_id',
            'id',
            'id',
            'sender_id')->where('accepted', 0)->get();
    }



}
